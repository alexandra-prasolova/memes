//
//  PasteUrlViewController.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 13.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class PasteUrlViewController: KeyboardAppearanceHandlerViewController {
    
    @IBOutlet private var buttonBottomConstraint: NSLayoutConstraint!
    @IBOutlet private var loadingIndicator: UIActivityIndicatorView!
    
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var textView: UITextField!
    @IBOutlet private var useButton: UIButton!
    @IBOutlet private var saveButton: UIButton!
    
    var model: PasteUrlModel!
    
    override var constraintStickingToKeyboard: NSLayoutConstraint? {
        return buttonBottomConstraint
    }
    
    override var lowercaseConstant: CGFloat { 21 }
    override var uppercaseConstant: CGFloat { 21 }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Paste image URL here", comment: ""), attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        navigationItem.title = NSLocalizedString("Image URL", comment: "")
        useButton.isEnabled = false
        saveButton.isEnabled = false
        model.observer = self
        textView.delegate = self
        startWatchingTapOutideTextInput()
        
        NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: .main) { _ in
            self.checkForURLInPasteboard()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        startKeyboardTracking()
        self.textView.becomeFirstResponder()
        checkForURLInPasteboard()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        stopKeyboardTracking()
    }
    
    private func checkForURLInPasteboard() {
        if let string = UIPasteboard.general.string, string.isImageURL{
            self.textView.text = string
            textFieldEdited(textView)
        }
    }
    
    @IBAction private func useButtonTapped(_ sender: Any) {
        model.useCurrentImage()
    }
    
    @IBAction private func saveTemplateButtonTapped(_ sender: Any) {
        model.saveCurrentImageTemplate()
    }
    
    @IBAction private func textFieldEdited(_ sender: UITextField) {
        let text = sender.text!
            if text.isImageURL, let url = URL(string: text) {
                self.model.setURL(url)
        }
    }
}

extension PasteUrlViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
}

extension PasteUrlViewController: PasteUrlModelObserver {
    
    func didFailToGainLibraryPermission(_ model: PasteUrlModel) {
        DispatchQueue.main.async {
            self.prompt(title: NSLocalizedString("Go to Settings?", comment: ""), subtitle: NSLocalizedString("Need your permission to save meme templates to library album", comment: ""), actionTitle: NSLocalizedString("Settings", comment: ""), acceptedBlock: {
                UIApplication.openSettings()
            })
        }
    }
    
    func didSaveImageTemplate(_ model: PasteUrlModel, error: String?) {
        DispatchQueue.main.async {
            if let errorText = error {
                self.showError(errorText: errorText)
            } else {
                self.showMessage(messageText: nil, title: NSLocalizedString("Image saved to Templates", comment: ""))
            }
        }
    }
    
    func startedLoading(_ model: PasteUrlModel) {
        loadingIndicator.startAnimating()
        imageView.image = nil
        useButton.isEnabled = false
        saveButton.isEnabled = false
    }
    
    func didFailImageLoad(_ model: PasteUrlModel, error: String) {
        loadingIndicator.stopAnimating()
        imageView.image = nil
        showError(errorText: error)
        useButton.isEnabled = false
        saveButton.isEnabled = false
    }
    
    func didReceiveImage(_ model: PasteUrlModel, image: UIImage) {
        imageView.image = image
        loadingIndicator.stopAnimating()
        useButton.isEnabled = true
        saveButton.isEnabled = true
    }
    
}

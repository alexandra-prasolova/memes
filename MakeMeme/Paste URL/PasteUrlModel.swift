//
//  PasteUrlModel.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 14.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

protocol PasteUrlModelObserver: class {
    func startedLoading(_ model: PasteUrlModel)
    func didFailImageLoad(_ model: PasteUrlModel, error: String)
    func didReceiveImage(_ model: PasteUrlModel, image: UIImage)
    func didSaveImageTemplate(_ model: PasteUrlModel, error: String?)
    func didFailToGainLibraryPermission(_ model: PasteUrlModel)
}

final class PasteUrlModel: ImageSource {
    
    weak var delegate: ImageSourceObserver?
    
    weak var observer: PasteUrlModelObserver?
     
    private(set) var imageService: ImageService
    private(set) var album: MemesPhotoAlbum
    private(set) var navigator: Navigator
    
    private var currentImage: UIImage?
    
    init(imageService: ImageService, navigator: Navigator, album: MemesPhotoAlbum) {
        self.imageService = imageService
        self.navigator = navigator
        self.album = album
        
        album.addObserver(self)
        imageService.addObserver(self)
    }
    
    func setURL(_ url: URL) {
        self.imageService.setURL(url)
    }
    
    func useCurrentImage() {
        if let image = currentImage {
            delegate?.didConfirmImage(self, image: image)
            navigator.back()
        }
    }
    
    func saveCurrentImageTemplate() {
        if let image = currentImage {
            album.saveImage(image: image)
        }
    }
    
}

extension PasteUrlModel: ImageServiceObserver {
    func didReceiveURL(_ service: ImageService, url: URL) {
        observer?.startedLoading(self)
        currentImage = nil
    }
    
    func didDownloadImage(_ service: ImageService, image: UIImage) {
        observer?.didReceiveImage(self, image: image)
        currentImage = image
    }
    
    func didFailImageDownload(_ service: ImageService, error: String) {
        observer?.didFailImageLoad(self, error: error)
        currentImage = nil
    }
    
    func didConfirmImage(_ service: ImageService, image: UIImage) {
        navigator.backHome()
    }
}

extension PasteUrlModel: MemesPhotoAlbumObserver {
    
    func didAddNewPhoto(_ album: MemesPhotoAlbum, photo: UIImage) {
        observer?.didSaveImageTemplate(self, error: nil)
    }
    
    func didUpdatePhotos(_ album: MemesPhotoAlbum, photos: [UIImage]) {}
    
    func didFailToSaveImage(_ album: MemesPhotoAlbum, errorText: String) {
        observer?.didSaveImageTemplate(self, error: errorText)
    }
    
    func didFailToGainLibraryPermission(_ album: MemesPhotoAlbum) {
        observer?.didFailToGainLibraryPermission(self)
    }
    
}

//
//  EditMemeView.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 26.03.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

protocol EditMemeViewDelegate: class {
    
    func editMemeViewTappedWhenEmpty(_ editMemeView: EditMemeView)
    
}

final class EditMemeView: UIView, NibLoadable {
    
    weak var delegate: EditMemeViewDelegate?
     
    var activeTextfieldFrame: CGRect {
        let label = labels.filter { $0.isActive }.first
        return label?.frame ?? .zero
    }
    
    var hasImage: Bool {
        return imageView.image != nil
    }
    
    var image: UIImage? {
        return imageView.image
    }
    
    private var ratioConstraint: NSLayoutConstraint!
    private var tapGesture: UITapGestureRecognizer?
    
    private var labels = [MemeLabel]()
    private let imageView = UIImageView(frame: .zero)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setup()
    }
    
    private func setup() {
        backgroundColor = #colorLiteral(red: 0.9999018312, green: 1, blue: 0.9998798966, alpha: 0.1580327181)
        addSubview(imageView)
        clipsToBounds = true
        setImage(nil)
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        tapGesture?.cancelsTouchesInView = false
        addGestureRecognizer(tapGesture!)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imageView.frame = bounds
    }
    
    func isPointInLabel(_ point: CGPoint) -> Bool {
        for label in labels {
            if label.frame.contains(point) {
                return true
            }
        }
        return false
    }
    
    func clearAll() {
        setImage(nil)
        labels.forEach { $0.removeFromSuperview()}
        labels = []
    }
    
    func setImage(_ image: UIImage?) {
        tapGesture?.isEnabled = image == nil
        imageView.image = image
        if ratioConstraint != nil {
            removeConstraint(ratioConstraint!)
        }
        var ratio: CGFloat = 1
        if let size = image?.size {
            ratio = size.height / size.width
        }
        let constraint =
        NSLayoutConstraint(item: self,
                           attribute: NSLayoutConstraint.Attribute.height,
                           relatedBy: NSLayoutConstraint.Relation.equal,
                           toItem: self,
                           attribute: NSLayoutConstraint.Attribute.width,
                           multiplier: ratio,
                           constant: 0)
        addConstraint(constraint)
        ratioConstraint = constraint
    }
    
    func getMeme() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(frame.size, false, 0.0)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    func addLabel() {
        let label = MemeLabel(approximateMemeWidth: bounds.width)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panLabelView))
        label.addGestureRecognizer(panGesture)
        label.panGesture = panGesture
        panGesture.cancelsTouchesInView = true
        
        let resizePanGesture = UIPanGestureRecognizer(target: self, action: #selector(panResizeControl))
        label.resizeControl.addGestureRecognizer(resizePanGesture)
        label.resizePanGesture = resizePanGesture
        resizePanGesture.cancelsTouchesInView = true
        
        label.deleteButton.addTarget(self, action: #selector(deleteLabel), for: .touchUpInside)
        
        addSubview(label)
        label.center = center
        labels.append(label)
        label.delegate = self
    }
    
    @objc
    private func viewTapped(_ sender: UITapGestureRecognizer) {
        guard imageView.image == nil else { return }
        let p = sender.location(in: self)
        let tappedLabels = labels.filter { $0.frame.contains(p) }
        guard tappedLabels.isEmpty else { return }
        endEditing(true)
        delegate?.editMemeViewTappedWhenEmpty(self)
    }
    
    @objc
    private func panLabelView(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: self)
        
        if let label = sender.view as? MemeLabel {
            label.isHeld = sender.state != .ended
            label.center = CGPoint(x: label.center.x + translation.x,
                y: label.center.y + translation.y)
            sender.setTranslation(CGPoint(x: 0, y: 0), in: label)
        }
    }
    
    @objc
    private func panResizeControl(_ sender: UIPanGestureRecognizer) {
        if let resizeControl = sender.view, let label = resizeControl.superview as? MemeLabel{
            let translation = sender.translation(in: label)
            
            let newWidth = label.frame.width + translation.x
            let newHeight = label.frame.height + translation.y
            
            guard newWidth > label.minDimention, newHeight > label.minDimention else { return }
            
            label.frame = CGRect(origin: label.frame.origin, size: CGSize(width: newWidth, height: newHeight))
            sender.setTranslation(CGPoint(x: 0, y: 0), in: resizeControl)
            label.updateTextFont()
        }
    }
    
    @objc
    private func deleteLabel(_ sender: Any) {
        guard let label = (sender as? UIView)?.superview as? MemeLabel else { return }
        
        UIView.animate(withDuration: 0.3,
                       delay: 0,
                       options: UIView.AnimationOptions.curveEaseIn,
                       animations: {
                        label.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
                        label.alpha = 0
        }, completion: { [weak self] _ in
            
            label.removeFromSuperview()
            if let i = self!.labels.firstIndex(of: label) {
                self!.labels.remove(at: i)
            }
        })
    }
    
}

extension EditMemeView: MemeLabelDelegate {
    func memeLabelRanOutOfSpace(_ memeLabel: MemeLabel) {
        var labelFrame = memeLabel.frame
        let heightSurplus = memeLabel.textView.font!.lineHeight+10
        let newSize = CGSize(width: labelFrame.width, height: labelFrame.height + heightSurplus)
        labelFrame.size = newSize
        if labelFrame.maxY + heightSurplus > frame.height {
            labelFrame.origin = CGPoint(x: labelFrame.origin.x, y: labelFrame.origin.y - heightSurplus)
        }
        memeLabel.frame = labelFrame
    }
}

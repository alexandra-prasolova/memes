//
//  GradientView.swift
//  Hat
//
//  Created by Alexandra Prasolova on 25.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class GradientView: UIView {
    
    private let gradient = CAGradientLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.insertSublayer(gradient, at: 0)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        layer.insertSublayer(gradient, at: 0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        gradient.frame = bounds
        gradient.colors = [tintColor.cgColor, tintColor.withAlphaComponent(0).cgColor]

        gradient.startPoint = CGPoint(x: 1, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 1)
    }
    
}

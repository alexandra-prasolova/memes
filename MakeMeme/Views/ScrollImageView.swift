//
//  ScrollImageView.swift
//  Memgo
//
//  Created by Alexandra Prasolova on 29.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

/// Intended to be used like UIImageView
final class ScrollImageView: UIView, NibLoadable {
    
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var imageHeight: NSLayoutConstraint!
    @IBOutlet private var imageWidth: NSLayoutConstraint!
    
    var image: UIImage? {
        set {
            imageView.image = newValue
            imageHeight.constant = newValue?.size.height ?? 100
            imageWidth.constant = newValue?.size.width ?? 100
            
            setupMinimumScale()
            toMinimumScale()
        }
        get {
            return imageView.image
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        scrollView.minimumZoomScale = 0.1
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupMinimumScale()
    }
    
    func toMinimumScale() {
        scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
    }
    
    func setScrollView(locked: Bool) {
        scrollView.isScrollEnabled = !locked
    }
    
    private func setupMinimumScale() {
        let hMin = scrollView.frame.width/imageWidth.constant
        let vMin = scrollView.frame.height/imageHeight.constant
        scrollView.minimumZoomScale = max(hMin, vMin)
        scrollView.setZoomScale(max(scrollView.minimumZoomScale, scrollView.zoomScale), animated: true)
    }
    
}

extension ScrollImageView: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
}

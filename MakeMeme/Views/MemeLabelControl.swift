//
//  MemeLabelControl.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 11.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class MemeControlView: UIButton {
    
    static let basicWidth: CGFloat = 38
    static let basicHeight: CGFloat = 38
    static let basicFrame = CGRect(x: 0, y: 0, width: basicWidth, height: basicHeight)
    
    static func resise() -> MemeControlView {
        let control = MemeControlView(frame: basicFrame)
        control.backgroundColor = UIColor.ivory.withAlphaComponent(1)
        control.titleLabel?.font = UIFont.systemFont(ofSize: 34)
        control.setTitleColor(.baseDark, for: .normal)
        control.setTitle("↘︎", for: .normal)
        control.makeMemable()
        return control
    }
    
    static func delete() -> MemeControlView {
        let control = MemeControlView(frame: basicFrame)
        control.titleLabel?.font = UIFont.systemFont(ofSize: 32)
        control.setTitleColor(.baseDark, for: .normal)
        control.setTitle("✕", for: .normal)
        control.backgroundColor = UIColor.attention.withAlphaComponent(1)
        control.makeMemable()
        return control
    }
    
    static func colorChange() -> MemeControlView {
        let control = MemeControlView(frame: basicFrame)
        control.setAttributedTitle(NSAttributedString(string: "M", attributes: [NSAttributedString.Key.font: UIFont(name: "impact", size: 23)!]), for: .normal)
        control.backgroundColor = UIColor.gray.withAlphaComponent(1)
        control.makeMemable()
        return control
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.makeMemable()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        self.makeMemable()
    }
    
    override func setTitleColor(_ color: UIColor?, for state: UIControl.State) {
        super.setTitleColor(color, for: state)
        
        if let title = self.attributedTitle(for: .normal) {
            let mutableTitle: NSMutableAttributedString = NSMutableAttributedString(attributedString: title)
            mutableTitle.addAttribute(.foregroundColor, value: color ?? .black, range: (mutableTitle.string as NSString).range(of: mutableTitle.string))
            setAttributedTitle(mutableTitle, for: .normal)
        }
    }
}

extension UIView {
    
    func makeMemable() {
        layer.borderWidth = 2
        layer.borderColor = UIColor.baseDark.cgColor
        layer.cornerRadius = 8
    }
    
}

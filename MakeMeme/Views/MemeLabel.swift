//
//  MemeLabel.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 01.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

protocol MemeLabelDelegate: class {
    func memeLabelRanOutOfSpace(_ memeLabel: MemeLabel)
}

final class MemeLabel: UIView, UITextViewDelegate {
    
    weak var delegate: MemeLabelDelegate?
    
    let resizeControl = MemeControlView.resise()
    let deleteButton = MemeControlView.delete()
    let colorControl = MemeControlView.colorChange()
    
    var panGesture: UIPanGestureRecognizer?
    var longPressGesture: UILongPressGestureRecognizer?
    var resizePanGesture: UIPanGestureRecognizer?
    
    var isHeld: Bool = false {
        didSet {
            containerView.backgroundColor = isHeld ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.4218225671) : .clear
        }
    }
    
    var isActive: Bool = false {
        didSet {
            let flag = isActive || textView.text.isEmpty
            containerView.layer.borderWidth = flag ? 2 : 0
            resizeControl.isHidden = !flag
            deleteButton.isHidden = !flag
            colorControl.isHidden = !flag
        }
    }
    
    var minDimention: CGFloat {
        return MemeControlView.basicWidth*2
    }
    
    private(set) var textView: UITextView = UITextView()
    private var containerView = UIView(frame: .zero)
    
    private var dashBorder: CAShapeLayer?
    
    private let minTextSize: CGFloat = 14
    private let maxTextSize: CGFloat = UIScreen.main.bounds.width/8
    
    init(approximateMemeWidth: CGFloat) {
        let startWidth = approximateMemeWidth / 1.5
        
        super.init(frame: CGRect(x: 0, y: 0, width: startWidth, height: max( MemeControlView.basicHeight*2 + 6, startWidth / 5)))
        textView.font = UIFont(name: "impact", size: startWidth / 7)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        containerView.frame = CGRect(x: MemeControlView.basicWidth/2, y: MemeControlView.basicHeight/2, width: bounds.width - MemeControlView.basicWidth, height: bounds.height - MemeControlView.basicHeight)
        textView.frame = CGRect(x: MemeControlView.basicWidth/2, y: 0, width: containerView.bounds.width - MemeControlView.basicWidth, height: containerView.bounds.height)
        
        let marginW = MemeControlView.basicWidth / 2
        let marginH = MemeControlView.basicHeight / 2
        resizeControl.center = CGPoint(x: frame.width - marginW, y: frame.height - marginH)
        deleteButton.center = CGPoint(x: frame.width - marginW, y: marginH)
        colorControl.center = CGPoint(x: marginW, y: marginH)
    }
    
    private func setup() {
        
        isHeld = false
        isActive = false
        isUserInteractionEnabled = true
        
        addSubview(containerView)
        containerView.backgroundColor = .clear
        containerView.makeMemable()
        
        containerView.addSubview(textView)
        textView.backgroundColor = .clear
        textView.delegate = self
        textView.autocorrectionType = .no
        textView.textColor = .white
        textView.tintColor = .attention
        
        textView.layer.shadowColor = UIColor.black.cgColor
        textView.layer.shadowRadius = 3
        textView.layer.shadowOffset = .zero
        textView.layer.shadowOpacity = 1
        textView.isScrollEnabled = false
        textView.textAlignment = .center
        
        colorControl.addTarget(self, action: #selector(switchTextColor), for: .touchUpInside)
        
        addSubview(resizeControl)
        addSubview(deleteButton)
        addSubview(colorControl)
    }
    
    func updateTextFont() {
        if (textView.text.isEmpty || textView.bounds.size == CGSize.zero) {
            return
        }

        let textViewSize = textView.frame.size
        let fixedWidth = textViewSize.width
        let expectSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT)))

        var expectFont = textView.font
        if (expectSize.height > textViewSize.height) {
            while (textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT))).height > textViewSize.height) {
                guard textView.font!.pointSize >= minTextSize else {
                    delegate?.memeLabelRanOutOfSpace(self)
                    return
                }
                expectFont = textView.font!.withSize(textView.font!.pointSize - 1)
                textView.font = expectFont
            }
        }
        else {
            while (textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT))).height < textViewSize.height) {
                guard textView.font!.pointSize <= maxTextSize else { return }
                expectFont = textView.font;
                textView.font = textView.font!.withSize(textView.font!.pointSize + 1)
            }
            textView.font = expectFont
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        updateTextFont()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        isActive = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        isActive = false
    }
    
    @objc private func switchTextColor() {
        switch textView.textColor! {
        case .white:
            textView.textColor = .black
            textView.layer.shadowColor = UIColor.white.cgColor
            colorControl.setTitleColor(.white, for: .normal)
        case .black:
            textView.textColor = .white
            textView.layer.shadowColor = UIColor.black.cgColor
            colorControl.setTitleColor(.black, for: .normal)
        default:
            break
        }
    }
    
}

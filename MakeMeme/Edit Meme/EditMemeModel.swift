//
//  EditMemeModel.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 26.03.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit
import AVFoundation

protocol EditMemeModelObserver: class {
    func didReceiveImage(_ model: EditMemeModel, image: UIImage)
    func didSaveImageTemplate(_ model: EditMemeModel, error: String?)
    func didFailToGainLibraryPermission(_ model: EditMemeModel)
}

final class EditMemeModel {
    
    weak var observer: EditMemeModelObserver?
    
    var dependenciesManager: DependenciesManager
    var navigator: Navigator
    var imageService: ImageService
    var album: MemesPhotoAlbum
    
    init(depManager: DependenciesManager, navigator: Navigator, imageService: ImageService, album: MemesPhotoAlbum) {
        self.dependenciesManager = depManager
        self.navigator = navigator
        self.imageService = imageService
        self.album = album
        
        album.addObserver(self)
        imageService.addObserver(self)
    }
    
    func checkCameraPermission(grantedBlock: @escaping EmptyBlock, deniedBlock: @escaping EmptyBlock) {
        let access = AVCaptureDevice.authorizationStatus(for: .video)
        
        switch access {
        case .authorized:
            DispatchQueue.main.async {
                grantedBlock()
            }
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
                if granted {
                    DispatchQueue.main.async {
                        grantedBlock()
                    }
                }
            })
        case .denied, .restricted:
            DispatchQueue.main.async {
                deniedBlock()
            }
        @unknown default:
            break
        }
    }
    
    func saveImageToTemplates(_ image: UIImage) {
        album.saveImage(image: image)
    }
    
    func goToTemplates() {
        navigator.goToTemplates(delegate: self)
    }
    
    func goToPasteURL() {
        navigator.goToPasteURL(delegate: self)
    }
    
    func goToAbout() {
        navigator.goToAbout()
    }
    
    func goToTiles() {
        navigator.goToTiles()
    }
    
}

extension EditMemeModel: ImageServiceObserver {
    
    func didConfirmImage(_ service: ImageService, image: UIImage) {
        observer?.didReceiveImage(self, image: image)
    }
    
}

extension EditMemeModel: ImageSourceObserver {
    
    func didConfirmImage(_ service: ImageSource, image: UIImage) {
        observer?.didReceiveImage(self, image: image)
    }
    
}

extension EditMemeModel: MemesPhotoAlbumObserver {
    
    func didFailToGainLibraryPermission(_ album: MemesPhotoAlbum) {
        observer?.didFailToGainLibraryPermission(self)
    }
    
    func didAddNewPhoto(_ album: MemesPhotoAlbum, photo: UIImage) {
        observer?.didSaveImageTemplate(self, error: nil)
    }
    
    func didFailToSaveImage(_ album: MemesPhotoAlbum, errorText: String) {
        observer?.didSaveImageTemplate(self, error: errorText)
    }
    
}

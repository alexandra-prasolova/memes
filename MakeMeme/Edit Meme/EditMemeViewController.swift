//
//  ViewController.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 24.03.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit
import StoreKit

final class EditMemeViewController: KeyboardAppearanceHandlerViewController {
    
    var model: EditMemeModel!
    
    weak var onboardingDelegate: OnboardingDelegate?

    @IBOutlet private var relativeMemeViewHeight: NSLayoutConstraint!
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var memeViewHwight: NSLayoutConstraint!
    @IBOutlet private var editMemeView: EditMemeView!
    @IBOutlet private var scrollViewBottomConstraint: NSLayoutConstraint!
    
    override var constraintStickingToKeyboard: NSLayoutConstraint? { scrollViewBottomConstraint }
    
    override var lowercaseConstant: CGFloat { 70.0 }
    override var uppercaseConstant: CGFloat { 0.0 }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startWatchingTapOutideTextInput()
        startKeyboardTracking()
        let contextMenuInteraction = UIContextMenuInteraction(delegate: self)
        editMemeView.addInteraction(contextMenuInteraction)
        
        model.observer = self
        editMemeView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        startKeyboardTracking()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        onboardingDelegate?.startOnboardingIfNeeded()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        stopKeyboardTracking()
    }
    
    @IBAction private func detailsTapped(_ sender: Any) {
        model.goToAbout()
    }
    
    @IBAction private func shareTapped(_ sender: Any) {
        guard editMemeView.image != nil else {
            showMessage(messageText: NSLocalizedString("You can't make meme without any image", comment: ""), title: nil)
            return
        }
        if let image = editMemeView.getMeme() {
            let imageShare: [UIImage] = [image]
            let activityViewController = UIActivityViewController(activityItems: imageShare, applicationActivities: nil)
            activityViewController.completionWithItemsHandler = { [weak self] activityType, completed, returnedItems, error in
                if completed, activityType == .some(.saveToCameraRoll) {
                    self?.showMessage(messageText: NSLocalizedString("Meme saved to your library!", comment: ""), title: nil) {
                        SKStoreReviewController.requestReview()
                    }
                } else {
                    SKStoreReviewController.requestReview()
                }
            }
            activityViewController.popoverPresentationController?.sourceView = view
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction private func useUrlTapped(_ sender: Any) {
        model.goToPasteURL()
    }
    
    @IBAction private func takePhotoTapped(_ sender: Any) {
        model.checkCameraPermission(
            grantedBlock:
            { [weak self] in
                guard UIImagePickerController.isSourceTypeAvailable(.camera) else { return }
                let imagePickerVC = UIImagePickerController.forPhoto
                imagePickerVC.delegate = self
                self?.present(imagePickerVC, animated: true)
            },
            deniedBlock: {
                self.prompt(title: NSLocalizedString("Go to Settings?", comment: ""), subtitle: NSLocalizedString("Need your permission to access camera", comment: ""), actionTitle: NSLocalizedString("Settings", comment: ""), acceptedBlock: {
                     UIApplication.openSettings()
                })
            })
    }
    
    @IBAction private func libraryTapped(_ sender: Any) {
        let imagePickerVC = UIImagePickerController.forLibrary
        imagePickerVC.delegate = self
        present(imagePickerVC, animated: true)
    }
    
    @IBAction private func templatesTapped(_ sender: Any) {
        model.goToTemplates()
    }
    
    @IBAction private func addLabelTapped(_ sender: Any) {
        editMemeView.addLabel()
    }
    
    override func keyboardWillShow(in rect: CGRect, animationDuration: TimeInterval) {
        relativeMemeViewHeight.isActive = false
        
        scrollView.isScrollEnabled = true
        let memeHeight = editMemeView.frame.height
        memeViewHwight.constant = memeHeight
        memeViewHwight.isActive = true

        let stickingConstraintConstant = rect.height - view.safeAreaInsets.bottom
        
        constraintStickingToKeyboard!.constant = stickingConstraintConstant + uppercaseConstant
        
        UIView.animate(withDuration: animationDuration, animations: {
            self.view.layoutIfNeeded()
        }, completion: { [weak self] _ in
            guard let self = self else { return }
            self.scrollView.flashScrollIndicators()
            var frameToShow = self.editMemeView.activeTextfieldFrame
            frameToShow.origin = CGPoint(x: frameToShow.origin.x, y: frameToShow.origin.y + 10)
            self.scrollView.scrollRectToVisible(frameToShow, animated: true)
        })
        
    }
    
    override func keyboardWillHide(in rect: CGRect, animationDuration: TimeInterval) {
        scrollView.isScrollEnabled = false
        constraintStickingToKeyboard!.constant = lowercaseConstant
        UIView.animate(withDuration: animationDuration, animations: {
            self.view.layoutIfNeeded()
        }, completion: { [weak self] _ in
            self?.memeViewHwight.isActive = false
            self?.relativeMemeViewHeight.isActive = true
        })
    }
    
}

// MARK: Context menu delegate

extension EditMemeViewController: UIContextMenuInteractionDelegate {
    
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        
        guard !editMemeView.isPointInLabel(location) else { return nil }
        
        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil, actionProvider: { actions -> UIMenu? in
            
            let actionClearAll = UIAction(title: NSLocalizedString("Clear all", comment: ""), image: UIImage(systemName: "trash"), attributes: [UIMenuElement.Attributes.destructive]) { _ in
                self.prompt(title: NSLocalizedString("Delete image and all labels?", comment: ""), subtitle: nil, actionTitle: NSLocalizedString("Delete", comment: ""), acceptedBlock: {
                    self.editMemeView.clearAll()
                })
            }
            let actionNewLabel = UIAction(title: NSLocalizedString("Add label", comment: ""), image: UIImage(systemName: "textbox")) { _ in
                self.editMemeView.addLabel()
            }
            
            let actionTiles = UIAction(title: NSLocalizedString("Advanced Editing", comment: ""), image: UIImage(systemName: "rectangle.grid.2x2")) { _ in
                self.model.goToTiles()
            }
            
            var actions: [UIAction] = [actionClearAll, actionNewLabel, actionTiles]
            
            if self.editMemeView.hasImage {
                let actionSaveBackground = UIAction(title: NSLocalizedString("Save to Templates", comment: ""), image: UIImage(systemName: "folder")) { _ in
                    self.model.saveImageToTemplates(self.editMemeView.image!)
                }
                actions.append(actionSaveBackground)
            }
            
            return UIMenu(title: NSLocalizedString("Meme actions", comment: ""), image: nil, identifier: nil, options: [], children: actions)
            
        })
    }
    
}

// MARK: Image picker delegate

extension EditMemeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.originalImage] as? UIImage else { return }
        editMemeView.setImage(image)
        
    }
    
}

// MARK: Edit meme model observer

extension EditMemeViewController: EditMemeModelObserver {
    
    func didReceiveImage(_ model: EditMemeModel, image: UIImage) {
        editMemeView.clearAll()
        editMemeView.setImage(image)
    }
    
    func didFailToGainLibraryPermission(_ model: EditMemeModel) {
        DispatchQueue.main.async {
            self.prompt(title: NSLocalizedString("Go to Settings?", comment: ""), subtitle: NSLocalizedString("Need your permission to save meme templates to library album", comment: ""), actionTitle: NSLocalizedString("Settings", comment: ""), acceptedBlock: {
                UIApplication.openSettings()
            })
        }
    }
    
    func didSaveImageTemplate(_ model: EditMemeModel, error: String?) {
        DispatchQueue.main.async {
            if let errorText = error {
                self.showError(errorText: errorText)
            } else {
                self.showMessage(messageText: nil, title: NSLocalizedString("Image saved to Templates", comment: ""))
            }
        }
    }
    
}

extension EditMemeViewController: EditMemeViewDelegate {
    func editMemeViewTappedWhenEmpty(_ editMemeView: EditMemeView) {
        model.goToTemplates()
    }
}

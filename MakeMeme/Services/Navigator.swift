//
//  Navigator.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 26.03.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class Navigator {

    weak var rootNavigationController: UINavigationController?
    
    weak var depManager: DependenciesManager!
    
    init(depManager: DependenciesManager) {
        self.depManager = depManager
    }

    func goToTemplates(delegate: ImageSourceObserver) {
        let templatesVC = UIStoryboard.gallery.instantiate(TemplatesContainerViewController.self)
        
        let model = depManager.templatesModel
        model.delegate = delegate
        let savedTemplatesVC = UIStoryboard.gallery.instantiate(SavedTemplatesViewController.self)
        savedTemplatesVC.model = model
        
        let hypeTemplatesVC = UIStoryboard.gallery.instantiate(HypeTemplatesViewController.self)
        hypeTemplatesVC.model = model
        
        templatesVC.savedTemplatesVC = savedTemplatesVC
        templatesVC.hypedTemplatesVC = hypeTemplatesVC
        
        rootNavigationController?.pushViewController(templatesVC, animated: true)
    }
    
    func goToPasteURL(delegate: ImageSourceObserver) {
        let pasteUrlViewController = UIStoryboard.main.instantiate(PasteUrlViewController.self)
        let model = depManager.pasteUrlModel
        model.delegate = delegate
        pasteUrlViewController.model = model
        rootNavigationController?.pushViewController(pasteUrlViewController, animated: true)
    }
    
    func goToAbout() {
        let aboutViewController = UIStoryboard.about.instantiate(AboutViewController.self)
        aboutViewController.navigator = self
        
        rootNavigationController?.pushViewController(aboutViewController, animated: true)
    }
    
    func goToToS() {
        let tosViewController = UIStoryboard.about.instantiate(TermsViewController.self)
        tosViewController.contentType = .tos
        
        rootNavigationController?.pushViewController(tosViewController, animated: true)
    }
    
    func goToPP() {
        let tosViewController = UIStoryboard.about.instantiate(TermsViewController.self)
        tosViewController.contentType = .pp
        
        rootNavigationController?.pushViewController(tosViewController, animated: true)
    }
    
    func goToTiles() {
        let tilesVC = UIStoryboard.main.instantiate(TilesViewController.self)
        tilesVC.tilesModel = depManager.tilesModel
        
        rootNavigationController?.pushViewController(tilesVC, animated: true)
    }
    
    func presentEditOnboarding(completion: (() -> Void)?) {
        let onboardingVC = UIStoryboard.onboarding.instantiate(EditingOnboardingViewController.self)
        rootNavigationController?.topViewController?.present(onboardingVC, animated: true, completion: completion)
    }
    
    func backHome() {
        rootNavigationController?.popToRootViewController(animated: true)
    }
    
    func back() {
        rootNavigationController?.popViewController(animated: true)
    }
}


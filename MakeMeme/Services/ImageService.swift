//
//  ImageService.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 13.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

protocol ImageServiceObserver: class {
    
    func didReceiveURL(_ service: ImageService, url: URL)
    func didDownloadImage(_ service: ImageService, image: UIImage)
    func didFailImageDownload(_ service: ImageService, error: String)
    func didConfirmImage(_ service: ImageService, image: UIImage)
    
}

extension ImageServiceObserver {
    func didReceiveURL(_ service: ImageService, url: URL) {}
    func didDownloadImage(_ service: ImageService, image: UIImage) {}
    func didFailImageDownload(_ service: ImageService, error: String) {}
    func didConfirmImage(_ service: ImageService, image: UIImage) {}
}

final class ImageService {
    
    private var observers = ObserversContainer<ImageServiceObserver>()
    
    func addObserver(_ observer: ImageServiceObserver) {
        observers.add(observer)
    }
    
    func removeObserver(_ observer: ImageServiceObserver) {
        observers.remove(observer)
    }
    
    func setImage(_ image: UIImage) {
        self.observers.enumerate { observer in
            observer.didDownloadImage(self, image: image)
        }
    }
    
    func confirmImage(_ image: UIImage) {
        self.observers.enumerate { observer in
            observer.didConfirmImage(self, image: image)
        }
    }
    
    func setURL(_ url: URL) {
        observers.enumerate { observer in
            observer.didReceiveURL(self, url: url)
        }
        downloadImage(from: url)
    }
    
    private func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    private func downloadImage(from url: URL) {
        getData(from: url) { data, response, error in
            if let data = data, error == nil, let image = UIImage(data: data) {
                DispatchQueue.main.async() {
                    self.observers.enumerate { observer in
                        observer.didDownloadImage(self, image: image)
                    }
                }
            } else {
                DispatchQueue.main.async() {
                    self.observers.enumerate { observer in
                        observer.didFailImageDownload(self, error: error?.localizedDescription ?? NSLocalizedString("Failed to load image from URL", comment: ""))
                    }
                }
            }
        }
    }
    
}

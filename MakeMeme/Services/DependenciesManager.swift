//
//  DependenciesManager.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 26.03.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

final class DependenciesManager {
    
    private(set) lazy var network = Network()
    
    private(set) lazy var imageService = ImageService()
    
    private(set) lazy var album = MemesPhotoAlbum()
    
    private(set) var navigator: Navigator!
    
    var templatesModel: TemplatesModel {
        return TemplatesModel(dependenciesManager: self, network: network, navigator: navigator, album: album, imageService: imageService)
    }
    
    var editMemeModel: EditMemeModel {
        return EditMemeModel(depManager: self, navigator: navigator, imageService: imageService, album: album)
    }
    
    var pasteUrlModel: PasteUrlModel {
        return PasteUrlModel(imageService: imageService, navigator: navigator, album: album)
    }
    
    var tilesModel: TilesModel {
        return TilesModel(navigator: navigator, imageService: imageService)
    }
    
    init() {
        self.navigator = Navigator(depManager: self)
    }
}
 

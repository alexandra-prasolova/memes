//
//  AlbumService.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 17.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Photos
import UIKit

protocol MemesPhotoAlbumObserver: class {
    func didUpdatePhotos(_ album: MemesPhotoAlbum, photos: [UIImage])
    func didFailToSaveImage(_ album: MemesPhotoAlbum, errorText: String)
    func didFailToGainLibraryPermission(_ album: MemesPhotoAlbum)
    func didAddNewPhoto(_ album: MemesPhotoAlbum, photo: UIImage)
    func didDeletePhoto(_ album: MemesPhotoAlbum, photo: UIImage, at index: Int, errorText: String?)
}

extension MemesPhotoAlbumObserver {
    func didUpdatePhotos(_ album: MemesPhotoAlbum, photos: [UIImage]) {}
    func didFailToSaveImage(_ album: MemesPhotoAlbum, errorText: String) {}
    func didFailToGainLibraryPermission(_ album: MemesPhotoAlbum) {}
    func didAddNewPhoto(_ album: MemesPhotoAlbum, photo: UIImage) {}
    func didDeletePhoto(_ album: MemesPhotoAlbum, photo: UIImage, at index: Int, errorText: String?) {}
}

final class MemesPhotoAlbum {
    
    static let albumName = "Memgo"
    
    private var assetCollection = PHAssetCollection()
    private var observers = ObserversContainer<MemesPhotoAlbumObserver>()
    
    private(set) var images: [UIImage] = [] {
        didSet {
            observers.enumerate{ $0.didUpdatePhotos(self, photos: images) }
        }
    }
    private var imagesWithIdentifiers: [(String, UIImage)] = []
    
    init() {
        if let assetCollection = self.fetchAssetCollectionForAlbum() {
            self.assetCollection = assetCollection
        }
        extractImages(from: self.assetCollection)
    }
    
    func addObserver(_ observer: MemesPhotoAlbumObserver) {
        observers.add(observer)
    }
    
    func removeObserver(_ observer: MemesPhotoAlbumObserver) {
        observers.remove(observer)
    }
    
    func saveImage(image: UIImage) {
        
        let deniedBlock = { [weak self] in
            guard let self = self else { return }
            self.observers.enumerate { $0.didFailToGainLibraryPermission(self) }
        }
        
        let grantedBlock = { [weak self] in
            guard let self = self else { return }
            if self.images.isEmpty {
                PHPhotoLibrary.shared().performChanges({
                    PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: MemesPhotoAlbum.albumName)
                }) {_,_ in}
            }
            
            PHPhotoLibrary.shared().performChanges({
                let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
                let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
                let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
                albumChangeRequest?.addAssets([assetPlaceholder] as NSFastEnumeration)
            }, completionHandler: { flag, error in
                if flag, error == nil {
                    self.images.append(image)
                    self.observers.enumerate { $0.didAddNewPhoto(self, photo: image) }
                } else {
                    self.observers.enumerate { $0.didFailToSaveImage(self, errorText: error?.localizedDescription ?? "Failed to save image to Memes album.") }
                }
            })
        }
        
        getPermission(grantedBlock: grantedBlock, deniedBlock: deniedBlock)
    }
    
    func deleteImage(at index: Int ) {
        if self.images.isEmpty { return }
        let assetsToDelete = PHAsset.fetchAssets(withLocalIdentifiers: [imagesWithIdentifiers[index].0], options: nil)
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.deleteAssets(assetsToDelete)
        }, completionHandler: { [weak self] flag, error in
            guard let self = self else { return }
            DispatchQueue.main.async {
                let image = self.images[index]
                if flag, error == nil {
                    self.imagesWithIdentifiers.remove(at: index)
                    self.images.remove(at: index)
                    self.observers.enumerate { $0.didDeletePhoto(self, photo: image, at: index, errorText: nil) }
                } else {
                    self.observers.enumerate { $0.didDeletePhoto(self, photo: image, at: index, errorText: error?.localizedDescription ?? "Failed to delete image from library") }
                }
            }
        })
    }
    
    private func fetchAssetCollectionForAlbum() -> PHAssetCollection? {
        self.images = []
        guard PHPhotoLibrary.authorizationStatus() == .authorized else {
            return nil
        }
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", MemesPhotoAlbum.albumName)
        let collection: PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
        
        let assetCollection = collection.firstObject
        
        return assetCollection
    }
    
    private func extractImages(from assetCollection: PHAssetCollection) {
        let assets = PHAsset.fetchAssets(in: assetCollection, options: nil)
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        assets.enumerateObjects({ asset, _, _ in
            
            PHImageManager.default().requestImage(for: asset, targetSize: CGSize(width: 1000, height: 1000), contentMode: PHImageContentMode.aspectFit, options: requestOptions, resultHandler: { (image, info) in
                
                if let image = image {
                    self.images.append(image)
                    self.imagesWithIdentifiers.append((asset.localIdentifier, image))
                }
            })
        })
        
    }
    
    private func getPermission(grantedBlock: @escaping EmptyBlock, deniedBlock: @escaping EmptyBlock) {
        
        func check(_ status: PHAuthorizationStatus) {
            switch status {
            case .authorized:
                grantedBlock()
            case .notDetermined:
                PHPhotoLibrary.requestAuthorization { status in
                    check(status)
                }
            case .restricted, .denied:
                deniedBlock()
            default:
                break
            }
        }
        let status = PHPhotoLibrary.authorizationStatus()
        check(status)
    }
    
}

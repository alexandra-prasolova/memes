//
//  TilesModel.swift
//  Memgo
//
//  Created by Alexandra Prasolova on 02.05.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

protocol TilesModelObserver: class {
    func didReceiveImage(_ model: TilesModel, image: UIImage)
}

final class TilesModel {
    
    weak var observer: TilesModelObserver?
    
    var navigator: Navigator
    var imageService: ImageService
    
    var images: [UIImage] = []
    
    init(navigator: Navigator, imageService: ImageService) {
        self.navigator = navigator
        self.imageService = imageService
    }
    
    func use(image: UIImage) {
        imageService.confirmImage(image)
        navigator.backHome()
    }
    
    func showTemplates() {
        navigator.goToTemplates(delegate: self)
    }
    
    func goToPasteURL() {
        navigator.goToPasteURL(delegate: self)
    }
    
    func showOnboardingIfNeeded() {
        if !UserDefaults.standard.bool(forKey: "CompletedEditingOnboarding") {
            navigator.presentEditOnboarding() {
                UserDefaults.standard.set(true, forKey: "CompletedEditingOnboarding")
            }
        }
    }
}

extension TilesModel: ImageSourceObserver {
    
    func didConfirmImage(_ service: ImageSource, image: UIImage) {
        observer?.didReceiveImage(self, image: image)
    }
    
}

//
//  TilesViewController.swift
//  Memgo
//
//  Created by Alexandra Prasolova on 30.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class TilesViewController: UIViewController {

    var tilesModel: TilesModel!
    
    @IBOutlet private var useButton: UIBarButtonItem!
    @IBOutlet private var slider: UISlider!
    @IBOutlet private var tileView: TilesView!
    @IBOutlet private var toolbar: UIToolbar!
    
    private let maxRatio: CGFloat = 3.0
    private var k: CGFloat = 0.125
    private var ratioConstraint: NSLayoutConstraint?
    
    private let scrollImageView = ScrollImageView.loadFromNib()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        toolbar.isHidden = true
        tilesModel.observer = self
        tileView.addSubview(scrollImageView)
        tileView.delegate = self
        
        k = (0.5 / (maxRatio - 1))
        addConstraint(for: 1)
        
        navigationItem.title = NSLocalizedString("Advanced Editing", comment: "")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollImageView.toMinimumScale()
        startWatchingTap()
        tilesModel.showOnboardingIfNeeded()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        scrollImageView.frame = tileView.bounds
    }
    
    @IBAction private func didSlide(_ sender: UISlider) {
        var ratio: CGFloat = 1.0
        if sender.value > 0.5 {
            // width > height
            let r = CGFloat(sender.value-0.5)/k + 1
            
            ratio = r
        } else {
            // height <= width
            let r = CGFloat(-sender.value+0.5)/k + 1
             
            ratio = 1/r
        }
        addConstraint(for: ratio)
    }
    
    @IBAction private func photoLibraryTapped(_ sender: Any) {
        let imagePickerVC = UIImagePickerController.forLibrary
        imagePickerVC.delegate = self
        present(imagePickerVC, animated: true)
    }
    
    @IBAction private func cameraTapped(_ sender: Any) {
        let imagePickerVC = UIImagePickerController.forPhoto
        imagePickerVC.delegate = self
        present(imagePickerVC, animated: true)
    }
    
    @IBAction private func urlTapped(_ sender: Any) {
        tilesModel.goToPasteURL()
    }
    
    @IBAction private func templatesTapped(_ sender: Any) {
        tilesModel.showTemplates()
    }
    
    @IBAction private func useTapped(_ sender: Any) {
        guard tileView.filled else {
            showMessage(messageText: NSLocalizedString("Fill all the tiles with images", comment: ""), title: NSLocalizedString("Meme template can't have empty spaces", comment: ""))
            return
        }
        tileView.deselectAll()
        if let image = tileView.getImage() {
            tilesModel.use(image: image)
        }
    }
    
    private func addConstraint(for ratio: CGFloat) {
        ratioConstraint?.isActive = false
        let constraint = NSLayoutConstraint(item: tileView!, attribute: .width, relatedBy: .equal, toItem: tileView, attribute: .height, multiplier: ratio, constant: 0)
        tileView.addConstraint(constraint)
        ratioConstraint = constraint
        constraint.isActive = true
    }
    
    private func startWatchingTap() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tappedView))
        view.addGestureRecognizer(tap)
    }
    
    @objc
    private func tappedView(_ sender: UIPanGestureRecognizer) {
        if !(sender.view is TilesView) {
            toolbar.isHidden = true
            tileView.deselectAll()
        }
        
    }
}

extension TilesViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.originalImage] as? UIImage else { return }
        tileView.setImageAtSelectedIndex(image)

    }
    
}

extension TilesViewController: TilesModelObserver {
    
    func didReceiveImage(_ model: TilesModel, image: UIImage) {
        tileView.setImageAtSelectedIndex(image)
    }
    
}

extension TilesViewController: TilesViewDelegate {
    
    func tilesView(_ tilesView: TilesView, tappedAt index: Int) {
        toolbar.isHidden = false
    }
    
    func tilesView(_ tilesView: TilesView, didDeselectAt index: Int) {
        toolbar.isHidden = true
    }
    
}

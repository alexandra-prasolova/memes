//
//  Tile.swift
//  Memgo
//
//  Created by Alexandra Prasolova on 12.05.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

protocol TileDelegate: class {
    func divide(tile: Tile, horizontally: Bool)
    func delete(tile: Tile)
    func didTap(tile: Tile)
}

final class Tile: NSObject {
    
    var tapGesture: UITapGestureRecognizer?
    
    weak var delegate: TileDelegate?
    
    var isSelected: Bool = false {
        didSet {
            if isSelected {
                imageView.layer.borderColor = UIColor.attention.withAlphaComponent(0.6).cgColor
            } else if imageView.image == nil {
                imageView.layer.borderColor = UIColor.tint.withAlphaComponent(0.6).cgColor
            } else {
                imageView.layer.borderColor = UIColor.clear.cgColor
            }
        }
    }
    
    var relativeX = 0.0
    var relativeY = 0.0
    var relativeWidth = 1.0
    var relativeHeight = 1.0
    
    private(set) var imageView = ScrollImageView.loadFromNib()
    
    override init() {
        super.init()
        
        imageView.backgroundColor = UIColor.randomDarkBackground()
        
        set(image: nil)
        imageView.layer.borderWidth = 2
        imageView.clipsToBounds = true
        imageView.layer.borderColor = UIColor.tint.withAlphaComponent(0.6).cgColor
        let contextMenuInteraction = UIContextMenuInteraction(delegate: self)
        imageView.addInteraction(contextMenuInteraction)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tileTapped))
        tapGesture = tap
        imageView.addGestureRecognizer(tap)
    }
    
    func set(image: UIImage?) {
        imageView.image = image
    }
    
    @objc private func tileTapped(sender: UITapGestureRecognizer) {
        delegate?.didTap(tile: self)
    }
    
}

extension Tile: UIContextMenuInteractionDelegate {
    
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        
        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil, actionProvider: { actions -> UIMenu? in
            
            let actionClearAll = UIAction(title: NSLocalizedString("Delete Tiles", comment: ""), image: UIImage(systemName: "rectangle"), attributes: [UIMenuElement.Attributes.destructive]) { _ in
                self.delegate?.delete(tile: self)
            }
            
            let actionNewLabel = UIAction(title: NSLocalizedString("Split Horizontally", comment: ""), image: UIImage(systemName: "square.split.1x2")) { _ in
                self.imageView.endEditing(true)
                DispatchQueue.main.asyncAfter(deadline: .now()+0.66) {
                    self.delegate?.divide(tile: self, horizontally: true)
                }
            }

            let actionTiles = UIAction(title: NSLocalizedString("Split Vertically", comment: ""), image: UIImage(systemName: "square.split.2x1")) { _ in
                self.imageView.endEditing(true)
                DispatchQueue.main.asyncAfter(deadline: .now()+0.66) {
                    self.delegate?.divide(tile: self, horizontally: false)
                }
            }

            let actions: [UIAction] = [actionClearAll, actionNewLabel, actionTiles]
            
            return UIMenu(title: NSLocalizedString("Actions", comment: ""), image: nil, identifier: nil, options: [], children: actions)
            
        })
    }
    
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, willDisplayMenuFor configuration: UIContextMenuConfiguration, animator: UIContextMenuInteractionAnimating?) {
        imageView.layer.borderWidth = 0
    }
    
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, willEndFor configuration: UIContextMenuConfiguration, animator: UIContextMenuInteractionAnimating?) {
        imageView.layer.borderWidth = 2
    }
}

extension UIColor {
    
    static func randomDarkBackground() -> UIColor {
        let c = UIColor.baseDark
        var red = CGFloat()
        var green = CGFloat()
        var blue = CGFloat()
        c.getRed(&red, green: &green, blue: &blue, alpha: nil)
        return UIColor(red: red * CGFloat.random(in: 1.3...1.8), green: green * CGFloat.random(in: 1.3...1.8), blue: blue * CGFloat.random(in: 1.3...1.8), alpha: 1)
    }
    
}

//
//  TilesView.swift
//  Memgo
//
//  Created by Alexandra Prasolova on 02.05.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

protocol TilesViewDelegate: class {
    func tilesView(_ tilesView: TilesView, tappedAt index: Int)
    func tilesView(_ tilesView: TilesView, didDeselectAt index: Int)
}

final class TilesView: UIView {
    
    weak var delegate: TilesViewDelegate?
    
    private(set) var filled: Bool = false
    
    private var tiles = [Tile]()
    
    private(set) var selectedTileIndex: Int?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let width = bounds.width
        let height = bounds.height
        
        tiles.forEach { tile in
            tile.imageView.frame = CGRect(x: CGFloat(tile.relativeX) * width,
                                      y: CGFloat(tile.relativeY) * height,
                                      width: CGFloat(tile.relativeWidth) * width,
                                      height: CGFloat(tile.relativeHeight) * height)
        }

    }
    
    private func setup() {
        DispatchQueue.main.async {
            let tile = self.newTile()
            self.tiles.append(tile)
            self.addSubview(tile.imageView)
        }
    }
    
    func getImage() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(frame.size, false, 0.0)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    func deselectAll() {
        tiles.forEach { $0.isSelected = false }
        selectedTileIndex = nil
    }
    
    func setImageAtSelectedIndex(_ image: UIImage) {
        guard let i = selectedTileIndex else { return }
        tiles[i].set(image: image)
        let emptyTiles = tiles.filter({ $0.imageView.image == nil })
        filled = emptyTiles.isEmpty
    }
    
    /// Does not check for correct index
    private func splitTile(at i: Int, horizontally: Bool) {
        let tile = tiles[i]
        let newTile = self.newTile()
        if horizontally {
            newTile.relativeWidth = tile.relativeWidth
            newTile.relativeHeight = tile.relativeHeight/2.0
            newTile.relativeX = tile.relativeX
            newTile.relativeY = tile.relativeY + newTile.relativeHeight
            tile.relativeHeight = tile.relativeHeight/2.0
        } else {
            newTile.relativeWidth = tile.relativeWidth/2.0
            newTile.relativeHeight = tile.relativeHeight
            newTile.relativeX = tile.relativeX + newTile.relativeWidth
            newTile.relativeY = tile.relativeY
            tile.relativeWidth = tile.relativeWidth/2.0
        }
        tiles.append(newTile)
        tiles[i] = tile
        self.addSubview(newTile.imageView)
    }
    
    private func newTile() -> Tile {
        let newTile = Tile()
        newTile.delegate = self
        return newTile
    }
    
}

extension TilesView: TileDelegate {
    
    func didTap(tile: Tile) {
        let tappedIndex = tiles.firstIndex(of: tile)
        if let i = selectedTileIndex, i != tappedIndex {
            tiles[i].isSelected = false
        }
        
        tile.isSelected = !tile.isSelected
        if !tile.isSelected {
            selectedTileIndex = nil
            delegate?.tilesView(self, didDeselectAt: 0)
        } else {
            selectedTileIndex = tappedIndex
            delegate?.tilesView(self, tappedAt: tiles.firstIndex(of: tile)!)
        }
    }
    
    func divide(tile: Tile, horizontally: Bool) {
        if let i = tiles.firstIndex(of: tile) {
            // Need delay for menu animation to end
            DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
                self.splitTile(at: i, horizontally: horizontally)
            })
            
        }
    }
    
    func delete(tile: Tile) {
        guard tiles.count > 1 else { return }
        for i in 1..<tiles.count {
            tiles[i].imageView.removeFromSuperview()
        }
        tiles = [tiles.first!]
        tiles.first?.relativeX = 0
        tiles.first?.relativeY = 0
        tiles.first?.relativeWidth = 1
        tiles.first?.relativeHeight = 1
        deselectAll()
    }
    
}

//
//  OnboardingFinalViewController.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 23.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

class OnboardingFinalViewController: UIViewController {
    
    weak var onboardingDelegate: OnboardingDelegate?
    
    @IBAction private func doneButtonTapped(_ sender: Any) {
        onboardingDelegate?.completeOnboarding()
    }
    
}

//
//  OnboardingPageViewController.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 22.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class OnboardingPageViewController: UIPageViewController {
    
    weak var onboardingDelegate: OnboardingDelegate?
    
    private var pages: [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .baseDark
        
        let isPhone = UIDevice.current.userInterfaceIdiom == .phone
        
        let models: [OnboardingViewController.Model] = [
            .init(image: isPhone ? UIImage(named: "onboarding phone 1")! : UIImage(named: "onboarding pad 1")!, title: "Use templates", subtitle: "Make a meme using one of preset hype templates", firstIcon: UIImage(systemName: "folder"), secondIcon: nil),
            .init(image: isPhone ? UIImage(named: "onboarding phone 2")! : UIImage(named: "onboarding pad 2")!, title: "Use your photos", subtitle: "Take photo or use one from your library to make it into a meme!", firstIcon: UIImage(systemName: "camera"), secondIcon: UIImage(systemName: "photo")),
            .init(image: isPhone ? UIImage(named: "onboarding phone 3")! : UIImage(named: "onboarding pad 3")!, title: "Copy URLs", subtitle: "Insert a URL for any image you find on the Internet and slap some text over it!", firstIcon: UIImage(systemName: "link"), secondIcon: nil),
            .init(image: isPhone ? UIImage(named: "onboarding phone 4")! : UIImage(named: "onboarding pad 4")!, title: "Press the meme", subtitle: "To see more actions longpress your meme", firstIcon: nil, secondIcon: nil),
            .init(image: isPhone ? UIImage(named: "onboarding phone 5")! : UIImage(named: "onboarding pad 5")!, title: "Save templates", subtitle: "You favourite templates are just one tap away - save them to your templates album!", firstIcon: UIImage(systemName: "folder"), secondIcon: nil)
        ]
        
        pages = models.map {
            let vc = UIStoryboard.onboarding.instantiate(OnboardingViewController.self)
            vc.model = $0
            return vc
        }
        
        let finalVC = UIStoryboard.onboarding.instantiate(OnboardingFinalViewController.self)
        finalVC.onboardingDelegate = onboardingDelegate
        pages.append(finalVC)
        
        dataSource = self
        
        setViewControllers([pages.first!], direction: .forward, animated: false, completion: nil)
    }
    
}

extension OnboardingPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard pages.count > previousIndex else {
            return nil
        }
        
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = pages.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return pages[nextIndex]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return pages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        guard let firstViewController = viewControllers?.first, let
            firstViewControllerIndex = pages.firstIndex(of: firstViewController) else {
                return 0
        }
        
        return firstViewControllerIndex
    }
    
}

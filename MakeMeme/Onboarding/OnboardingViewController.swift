//
//  OnboardingViewController.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 22.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class OnboardingViewController: UIViewController {
    
    struct Model {
        var image: UIImage
        var title: String
        var subtitle: String
        var firstIcon: UIImage?
        var secondIcon: UIImage?
    }
    
    var model: Model? {
        didSet {
            if model != nil {
                group?.leave()
            }
        }
    }
    
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var subTitleLabel: UILabel!
    @IBOutlet private var firstIconImageView: UIImageView!
    @IBOutlet private var secondIconImageView: UIImageView!
    
    private var group: DispatchGroup? = DispatchGroup()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        group?.enter()
        group?.enter()
        group?.notify(queue: .main) { [weak self] in
            self?.set(model: self?.model)
            self?.model = nil
            self?.group = nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        group?.leave()
    }
    
    func set(model: Model?) {
        guard let model = model else { return }
        imageView.image = model.image
        titleLabel.text = NSLocalizedString(model.title, comment: "")
        subTitleLabel.text = NSLocalizedString(model.subtitle, comment: "")
        if let firstImage = model.firstIcon {
            firstIconImageView.isHidden = false
            firstIconImageView.image = firstImage
        } else {
            firstIconImageView.isHidden = true
        }
        if let secondImage = model.secondIcon {
            secondIconImageView.isHidden = false
            secondIconImageView.image = secondImage
        } else {
            secondIconImageView.isHidden = true
        }
    }

}

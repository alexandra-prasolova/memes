//
//  EditingOnboardingViewController.swift
//  Memgo
//
//  Created by Alexandra Prasolova on 15.05.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class EditingOnboardingViewController: UIViewController {
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

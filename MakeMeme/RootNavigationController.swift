//
//  RootNavigationController.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 25.03.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

protocol OnboardingDelegate: class {
    func completeOnboarding()
    func startOnboardingIfNeeded()
}

final class RootNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    private func startOnboarding() {
        let vc = UIStoryboard.onboarding.instantiate(OnboardingPageViewController.self)
        vc.modalPresentationStyle = .fullScreen
        vc.onboardingDelegate = self
        
        viewControllers.first?.present(vc, animated: true, completion: nil)
    }
    
    private func dismissOnboarding() {
        presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    private func setup() {
        navigationBar.barTintColor = .baseDark
        guard let editMemeVC = viewControllers.first as? EditMemeViewController else { return }
        let dependenciesManager = DependenciesManager()
        dependenciesManager.navigator.rootNavigationController = self
        editMemeVC.model = dependenciesManager.editMemeModel
        editMemeVC.onboardingDelegate = self
    }
    
}
extension RootNavigationController: OnboardingDelegate {
    
    func startOnboardingIfNeeded() {
        if !UserDefaults.standard.bool(forKey: "CompletedOnboarding") {
            startOnboarding()
        }
    }
   
    func completeOnboarding() {
        UserDefaults.standard.set(true, forKey: "CompletedOnboarding")
        dismissOnboarding()
    }
    
}

//
//  UIVIewController+Alerts.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 18.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

public extension UIViewController {
    
    func prompt(title: String, subtitle: String?, actionTitle: String, acceptedBlock: @escaping ()->Void) {
        let alertController = UIAlertController (title: title, message: subtitle, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: actionTitle, style: .default) { _ in
            acceptedBlock()
        }
        
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showError(errorText: String) {
        let alertController = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: errorText, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showMessage(messageText: String?, title: String?, completion: EmptyBlock? = nil) {
        let alertController = UIAlertController(title: title, message: messageText, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "OK", style: .default) { _ in
            completion?()
        }
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
}

//
//  HypeTemplatesViewController.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 20.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class HypeTemplatesViewController: UIViewController {
    
    var model: TemplatesModel!
    
    @IBOutlet private var collectionView: UICollectionView!
    
    private var images = HypeMemesFetcher.fetchTemplates()

    override func viewDidLoad() {
        super.viewDidLoad()

        if let layout = collectionView?.collectionViewLayout as? WaterfallLayout {
            layout.delegate = self
        }
        collectionView.register(TemplatesCell.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView.reloadData()
    }
    
}
//MARK: - CollectionView data source

extension HypeTemplatesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(at: indexPath, cell: TemplatesCell.self)
        cell.imageView.image = images[indexPath.item]
        return cell
    }
}

// MARK: - CollectionView delegate, layout

extension HypeTemplatesViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        model.select(images[indexPath.item])
        model.back()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let imageSize = images[indexPath.item].size
        let width = collectionView.bounds.width / 2 - 8
        let height = width * imageSize.height / imageSize.width
        return CGSize(width: width, height: height)
    }
    
}

// MARK: - Waterfall layout
extension HypeTemplatesViewController: WaterfallLayoutDelegate {
    
    func collectionViewLayout(_ layout: WaterfallLayout, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        let columnWidth = layout.columnWidth - layout.cellPadding*2
        let count = images.count
        guard count > 0  else {
            return columnWidth
        }
        switch indexPath.item {
        case 0..<count:
            let imageSize = images[indexPath.item].size
            return (imageSize.height * columnWidth) / imageSize.width + layout.cellPadding
        default:
            return columnWidth
        }
    }
    
}

// MARK: - Templates list
final class HypeMemesFetcher {
    
    static func fetchTemplates() -> [UIImage] {
        return [
            UIImage(named: "alwayshasbeen")!,
            UIImage(named: "driveby")!,
            UIImage(named: "leogastby")!,
            UIImage(named: "leodjango")!,
            UIImage(named: "doggos")!,
            UIImage(named: "catwaffle")!,
            UIImage(named: "woman cat")!,
            UIImage(named: "harold")!,
            UIImage(named: "funeral")!,
            UIImage(named: "osborn")!,
            UIImage(named: "damngina")!,
            UIImage(named: "drake")!,
            UIImage(named: "hethinking")!,
            UIImage(named: "awkward monkey")!,
            UIImage(named: "awkward monkey mask")!,
            UIImage(named: "is this")!,
            UIImage(named: "and its gone")!,
            UIImage(named: "combucha girl")!,
            UIImage(named: "iq")!,
            UIImage(named: "Black-guy-hiding-behind-tree")!,
            UIImage(named: "don")!,
            UIImage(named: "drink")!,
            UIImage(named: "chereshnya")!,
            UIImage(named: "hello")!,
            UIImage(named: "fat can in window")!,
            UIImage(named: "fry")!,
            UIImage(named: "directed by")!,
            UIImage(named: "girl teeth")!,
            UIImage(named: "derp cat")!,
            UIImage(named: "cat choke")!,
            UIImage(named: "kim bed")!,
            UIImage(named: "kim cry")!,
            UIImage(named: "kim ripped")!,
            UIImage(named: "painting-meme")!,
            UIImage(named: "parrot yell")!,
            UIImage(named: "loading cat")!,
            UIImage(named: "yelling cat")!,
            UIImage(named: "patrik")!,
            UIImage(named: "pressuasion")!,
            UIImage(named: "seagull yell")!,
            UIImage(named: "shit on fire")!,
            UIImage(named: "shrug girl")!,
            UIImage(named: "sleepy cat")!,
            UIImage(named: "spidey sick")!,
            UIImage(named: "spongebob mock")!,
            UIImage(named: "spongebob rainbow")!,
            UIImage(named: "stoned black dude")!,
            UIImage(named: "stoned fox")!,
            UIImage(named: "stoned kid")!,
            UIImage(named: "success kid")!,
            UIImage(named: "throw away")!,
            UIImage(named: "two spidies")!,
            UIImage(named: "u-suka")!,
            UIImage(named: "woman squint")!,
            UIImage(named: "chipseky")!,
            UIImage(named: "aliens")!,
            UIImage(named: "penguin1")!,
            UIImage(named: "penguin2")!,
            UIImage(named: "confession bear")!,
        ]
    }
    
}

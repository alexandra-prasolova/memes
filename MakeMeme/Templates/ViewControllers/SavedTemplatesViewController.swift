//
//  SavedTemplatesViewController.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 26.03.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class SavedTemplatesViewController: UIViewController {
    
    var model: TemplatesModel!
       
    @IBOutlet private var emptyStateLabel: UILabel!
    @IBOutlet private var collectionView: UICollectionView!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        model.observer = self
        if let layout = collectionView?.collectionViewLayout as? WaterfallLayout {
            layout.delegate = self
        }
        collectionView.register(TemplatesCell.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        emptyStateLabel.isHidden = !model.images.isEmpty
        collectionView.reloadData()
    }
    
}

// MARK: - Model observer

extension SavedTemplatesViewController: TemplatesModelObserver {
    
    func didDeletePhoto(_ model: TemplatesModel, at index: Int, errorText: String?) {
        if let error = errorText {
            showError(errorText: error)
        } else {
            collectionView.deleteItems(at: [IndexPath(item: index, section: 0)])
        }
    }
    
    func didFailToGainLibraryPermission(_ model: TemplatesModel) {
        DispatchQueue.main.async {
            self.prompt(title: NSLocalizedString("Go to Settings?", comment: ""), subtitle: NSLocalizedString("Need your permission to save meme templates to library album", comment: ""), actionTitle: NSLocalizedString("Settings", comment: ""), acceptedBlock: {
                UIApplication.openSettings()
            })
        }
    }
    
    func didFailToSaveImage(_ model: TemplatesModel, errorText: String) {
        debugPrint(errorText)
    }
    
    func didUpdatePhotos(_ model: TemplatesModel, photos: [UIImage]) {
        DispatchQueue.main.async {
            self.emptyStateLabel.isHidden = !model.images.isEmpty
            self.collectionView.reloadData()
        }
    }
    
}

//MARK: - CollectionView data source

extension SavedTemplatesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(at: indexPath, cell: TemplatesCell.self)
        cell.imageView.image = model.images[indexPath.item]
        return cell
    }
}

// MARK: - CollectionView delegate, layout

extension SavedTemplatesViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        model.select(at: indexPath.item)
        model.back()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let imageSize = model.images[indexPath.item].size
        let width = collectionView.bounds.width / 2 - 8
        let height = width * imageSize.height / imageSize.width
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, contextMenuConfigurationForItemAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        let configuration = UIContextMenuConfiguration(identifier: nil, previewProvider: nil, actionProvider: { actions -> UIMenu? in
            let deleteAction = UIAction(title: NSLocalizedString("Delete", comment: ""), image: UIImage(systemName: "trash"), attributes: [UIMenuElement.Attributes.destructive]) { _ in
                self.model.delete(at: indexPath.item)
            }
            return UIMenu(title: NSLocalizedString("Image actions", comment: ""), image: nil, identifier: nil, options: [], children: [deleteAction])
        })
        return configuration
    }
    
}

// MARK: - Waterfall layout
extension SavedTemplatesViewController: WaterfallLayoutDelegate {
    
    func collectionViewLayout(_ layout: WaterfallLayout, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        let columnWidth = layout.columnWidth - layout.cellPadding*2
        let count = model.images.count
        guard count > 0  else {
            return columnWidth
        }
        switch indexPath.item {
        case 0..<count:
            let imageSize = model.images[indexPath.item].size
            return (imageSize.height * columnWidth) / imageSize.width + layout.cellPadding
        default:
            return columnWidth
        }
    }
    
}

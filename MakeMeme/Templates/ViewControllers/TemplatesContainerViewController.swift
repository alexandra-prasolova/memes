//
//  TemplatesContainerViewController.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 20.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

enum GalleryType {
    case hype
    case saved
}

final class TemplatesContainerViewController: UIViewController {

    @IBOutlet private weak var resultContainerView: UIView!
    @IBOutlet private weak var tabBar: TopTabBar!
    
    private var galleryType: GalleryType = .hype {
        didSet {
            showResults(for: galleryType)
        }
    }
    
    private var selectedResultVC: UIViewController {
        switch galleryType {
        case .hype:
            return hypedTemplatesVC
        case .saved:
            return savedTemplatesVC
        }
    }
    
    var hypedTemplatesVC: HypeTemplatesViewController!
    
    var savedTemplatesVC: SavedTemplatesViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBar.delegate = self
        tabBar.setup(with: [.hype, .saved])
        tabBar.selectItem(.hype)
        
        navigationItem.title = NSLocalizedString("Templates", comment: "")
    }
    
    private func showResults(for galleryType: GalleryType) {
        switch galleryType {
        case .saved:
            removeResutsVC(hypedTemplatesVC)
            addResultsVC(savedTemplatesVC)
        case .hype:
            removeResutsVC(savedTemplatesVC)
            addResultsVC(hypedTemplatesVC)
        }
    }
    
    private func addResultsVC(_ feedVC: UIViewController) {
        if !children.contains(feedVC) {
            removeResutsVC(feedVC)
        }
        addChild(feedVC)
        resultContainerView.addSubview(feedVC.view)
        feedVC.view.frame = resultContainerView.bounds
        feedVC.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        feedVC.didMove(toParent: self)
    }
    
    private func removeResutsVC(_ feedVC: UIViewController) {
        guard children.contains(feedVC) else { return }
        feedVC.willMove(toParent: nil)
        feedVC.view.removeFromSuperview()
        feedVC.removeFromParent()
    }

}

extension TemplatesContainerViewController: TopTabBarDelegate {
    
    func topTabBar(_ tabBar: TopTabBar, didSelectItem item: TopTabItem, atIndex index: Int) {
        switch item {
        case .hype:
            galleryType = .hype
        default:
            galleryType = .saved
        }
    }
    
}

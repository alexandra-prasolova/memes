//
//  WaterfallLayout.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 19.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

@objc protocol WaterfallLayoutDelegate: class {
    func collectionViewLayout(_ layout: WaterfallLayout, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat
    @objc optional func collectionViewLayoutHeightForHeader(_ layout: WaterfallLayout) -> CGFloat
}

class WaterfallLayout: UICollectionViewLayout {
    
    weak var delegate: WaterfallLayoutDelegate!
    var numberOfColumns = 2
    var cellPadding: CGFloat = 4
    private(set) var columnWidth: CGFloat = 0
    
    // Array to keep a cache of attributes.
    private var cache = [UICollectionViewLayoutAttributes]()
    private var headerAttributes: UICollectionViewLayoutAttributes?
    
    private var contentHeight: CGFloat = 0
    private var contentWidth: CGFloat {
        guard let collectionView = collectionView else {
            return 0
        }
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left + insets.right)
    }
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    override func prepare() {
       
        guard let collectionView = collectionView else { return }
        
        if let height = delegate.collectionViewLayoutHeightForHeader?(self) {
            let frame = CGRect(x: 0, y: 0, width: contentWidth, height: height)
            let headerIndexPath = IndexPath(item: 0, section: 0)
            let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, with: headerIndexPath)
            attributes.frame = frame
            headerAttributes = attributes
        }

        cache = []
        // Pre-Calculates the X Offset for every column and adds an array to increment the currently max Y Offset for each column
        columnWidth = contentWidth / CGFloat(numberOfColumns)
        var xOffsets = [CGFloat]()
        for column in 0 ..< numberOfColumns {
            xOffsets.append(CGFloat(column) * columnWidth)
        }
        var column = 0
        var yOffset = [CGFloat](repeating: (headerAttributes?.frame.maxY ?? 0) + 16, count: numberOfColumns)
        
        // Iterates through the list of items in the first section
        for item in 0 ..< collectionView.numberOfItems(inSection: 0) {
            
            let indexPath = IndexPath(item: item, section: 0)
            
            // Asks the delegate for the height of the picture and the annotation and calculates the cell frame.
            let height = delegate.collectionViewLayout(self, heightForPhotoAtIndexPath: indexPath)
            let frame = CGRect(x: xOffsets[column], y: yOffset[column], width: columnWidth, height: height)
            let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
            
            // Creates an UICollectionViewLayoutItem with the frame and add it to the cache
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = insetFrame
            cache.append(attributes)
            
            // Updates the collection view content height
            contentHeight = max(contentHeight, frame.maxY)
            yOffset[column] = yOffset[column] + height
            
            column = column < (numberOfColumns - 1) ? (column + 1) : 0
        }
    }
    
    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        if elementKind == UICollectionView.elementKindSectionHeader {
            return headerAttributes
        }
        return nil
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var visibleLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        if let headerAttributes = headerAttributes, headerAttributes.frame.intersects(rect) {
            visibleLayoutAttributes.append(headerAttributes)
        }
        
        // Loop through the cache and look for items in the rect
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        return visibleLayoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[indexPath.item]
    }
    
}

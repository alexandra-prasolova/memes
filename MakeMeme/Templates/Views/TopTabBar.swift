//
//  TopTabBar.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 20.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

enum TopTabItem: String {
    case hype = "Hype"
    case saved = "My Saved"
}

protocol TopTabBarDelegate: class {
    func topTabBar(_ tabBar: TopTabBar, didSelectItem item: TopTabItem, atIndex index: Int)
}

class TopTabBar: UIView {
    
    private let barHeight: CGFloat = 2
    
    weak var delegate: TopTabBarDelegate?
    
    private(set) var selectedIndex: Int?
    
    private var selectionBar = UIView(frame: .init(origin: .zero, size: .init(width: 50, height: 2)))
    private var stackView = UIStackView(frame: .zero)
    
    var selectedItem: TopTabItem? {
        guard let selectedIndex = selectedIndex, selectedIndex < items.count else { return nil }
        return items[selectedIndex]
    }
    
    private var itemButtons: [TopTabBarButton] = []
    private var items: [TopTabItem] = [] {
        didSet {
            reloadItems()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        addSubview(stackView)
        selectionBar.backgroundColor = .ivory
        selectionBar.layer.cornerRadius = barHeight/2
        addSubview(selectionBar)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        stackView.frame = bounds
    }
    
    private func updateBarFrame() {
        var frame: CGRect
        if let i = selectedIndex {
            let barWidth = bounds.width / CGFloat(items.count)
            frame = CGRect(x: barWidth*CGFloat(i), y: bounds.height-barHeight, width: barWidth, height: barHeight)
        } else {
            frame = .zero
        }
        let prevFrame = selectionBar.frame
        
        UIView.animate(withDuration: prevFrame != .zero ? 0.3 : 0) {
            self.selectionBar.frame = frame
        }
    }
    
    func setup(with items: [TopTabItem]) {
        self.items = items
        stackView.distribution = (items.count > 2) ? .equalSpacing : .fillEqually
    }
    
    func selectItem(_ item: TopTabItem) {
        // Deselect old item
        if let selectedIndex = selectedIndex, itemButtons.count > selectedIndex  {
            itemButtons[selectedIndex].isSelected = false
        }

        // Select new item
        guard let index = items.firstIndex(of: item) else { return }
        itemButtons[index].isSelected = true
        selectedIndex = index
            
        guard let selectedItem = selectedItem else { return }
        delegate?.topTabBar(self, didSelectItem: selectedItem, atIndex: index)
        
        updateBarFrame()
    }
    
    private func reloadItems() {
        // Clear old items
        for itemButton in itemButtons {
            itemButton.removeFromSuperview()
        }
        itemButtons = []
        
        // Fill with new items
        for item in items {
            let itemButton = TopTabBarButton(item: item)
            itemButton.onTouchUpInside = { [weak self] (button) in
                self?.onTouchUpInside(button)
            }
            stackView.addArrangedSubview(itemButton)
            itemButtons.append(itemButton)
        }
        layoutIfNeeded()
    }
    
    private func onTouchUpInside(_ button: TopTabBarButton) {
        selectItem(button.item)
    }
}

final class TopTabBarButton: UIButton {

    var onTouchUpInside: ((TopTabBarButton) -> ())?

    private(set) var item: TopTabItem
        
    override var isSelected: Bool {
        didSet {
            if isSelected {
                titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .medium)
            } else {
                titleLabel?.font = UIFont.systemFont(ofSize: 17)
            }
        }
    }
    
    init(item: TopTabItem) {
        self.item = item
        super.init(frame: CGRect(origin: .zero, size: CGSize(width: 44, height: 44)))
        
        setTitleColor(UIColor.tint.withAlphaComponent(0.61), for: .normal)
        setTitleColor(.tint, for: .selected)
        setTitle(NSLocalizedString(item.rawValue, comment: ""), for: .normal)
        addTarget(self, action: #selector(didTouchUpInside), for: .touchUpInside)
        isSelected = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func didTouchUpInside() {
        onTouchUpInside?(self)
    }
}

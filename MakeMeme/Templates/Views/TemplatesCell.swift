//
//  TemplatesCell.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 19.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class TemplatesCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageView.layer.cornerRadius = 8
    }
    
}

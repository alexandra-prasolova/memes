//
//  TemplatesModel.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 26.03.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

protocol ImageSourceObserver: class {
    func didConfirmImage(_ service: ImageSource, image: UIImage)
}

protocol ImageSource {
    var delegate: ImageSourceObserver? { set get }
}

protocol TemplatesModelObserver: class {
    func didUpdatePhotos(_ model: TemplatesModel, photos: [UIImage])
    func didFailToSaveImage(_ model: TemplatesModel, errorText: String)
    func didFailToGainLibraryPermission(_ model: TemplatesModel)
    func didDeletePhoto(_ model: TemplatesModel, at index: Int, errorText: String?)
}

final class TemplatesModel: ImageSource {
    
    weak var observer: TemplatesModelObserver?
    weak var delegate: ImageSourceObserver?
    
    var dependenciesManager: DependenciesManager
    var navigator: Navigator
    var network: Network
    var album: MemesPhotoAlbum
    
    var images: [UIImage] {
        return album.images
    }
    
    init(dependenciesManager: DependenciesManager, network: Network, navigator: Navigator, album: MemesPhotoAlbum, imageService: ImageService) {
        self.dependenciesManager = dependenciesManager
        self.navigator = navigator
        self.network = network
        self.album = album
        album.addObserver(self)
    }
    
    func select(at index: Int) {
        delegate?.didConfirmImage(self, image: album.images[index])
    }
    
    func select(_ image: UIImage) {
        delegate?.didConfirmImage(self, image: image)
    }
    
    func delete(at index: Int) {
        album.deleteImage(at: index)
    }
    
    func back() {
        navigator.back()
    }
    
}

extension TemplatesModel: MemesPhotoAlbumObserver {
    
    func didDeletePhoto(_ album: MemesPhotoAlbum, photo: UIImage, at index: Int, errorText: String?) {
        observer?.didDeletePhoto(self, at: index, errorText: errorText)
    }
    
    func didFailToGainLibraryPermission(_ album: MemesPhotoAlbum) {
        observer?.didFailToGainLibraryPermission(self)
    }
    
    func didFailToSaveImage(_ album: MemesPhotoAlbum, errorText: String) {
        observer?.didFailToSaveImage(self, errorText: errorText)
    }
    
    func didUpdatePhotos(_ album: MemesPhotoAlbum, photos: [UIImage]) {
        observer?.didUpdatePhotos(self, photos: photos)
    }
}

//
//  AboutViewController.swift
//  MakeMeme
//
//  Created by Alexandra Prasolova on 24.04.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit
import MessageUI

final class AboutViewController: UIViewController {
    
    var navigator: Navigator!
    
    private struct AboutData {
        let title: String
    }

    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var versionLabel: UILabel!
    
    private var data: [AboutData] = [AboutData(title: NSLocalizedString("Terms and Conditions", comment: "")), AboutData(title: NSLocalizedString("Privacy Policy", comment: "")), AboutData(title: NSLocalizedString("Email Author 🌷", comment: ""))]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = NSLocalizedString("About Memgo", comment: "")
        setAppVersion()
        tableView.register(AboutTableViewCell.self)
    }
    
    private func setAppVersion() {
        if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            versionLabel.text = "Memgo " + appVersion
        }
    }
    
    private func openMail() {
        guard MFMailComposeViewController.canSendMail() else { return }
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self

        composeVC.setToRecipients(["alexandra.volnaya@gmail.com"])
        composeVC.setSubject("Memgo")
        composeVC.setMessageBody("", isHTML: false)

        self.present(composeVC, animated: true, completion: nil)
    }
}

extension AboutViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            navigator.goToToS()
        case 1:
            navigator.goToPP()
        default:
            openMail()
        }
    }
    
}

extension AboutViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(at: indexPath, cell: AboutTableViewCell.self)
        cell.titleLabel?.text = data[indexPath.row].title
        
        return cell
    }
    
}

extension AboutViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        if let errorText = error?.localizedDescription {
            showError(errorText: errorText)
        }
    }
    
}

Hi! This is my pet-project, it has no commercial purpose and was created just for fun 😜 and also:

- Follows MVC architecture
- Was never meant to be shown to people
- Has some quick solutions that better be avoided in commercial apps
- Has no dependencies